//
// Copyright (c) 2018, Boris Popov <popov@whitekefir.ru>
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
//

#include <cnf/Layer.hpp>

#ifndef __cnflgr_Builder__
#define __cnflgr_Builder__

namespace cnflgr {
//////////////////////////////////////////////////////////////////
     class Builder {

     public:
	  Builder(const cnf::Layer&);

	  void build() const;

     private:
	  const cnf::Layer& layer;
     };
//////////////////////////////////////////////////////////////////
}
#endif // __cnflgr_Builder__
