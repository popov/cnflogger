//
// Copyright (c) 2018, Boris Popov <popov@whitekefir.ru>
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
//

#include "Logger.hpp"

#ifndef __cnflgr_Holder__
#define __cnflgr_Holder__

namespace cnflgr {
//////////////////////////////////////////////////////////////////
     class Holder {

     public:
	  static logger_t get(name_t);
     };
//////////////////////////////////////////////////////////////////
}
#endif // __cnflgr_Holder__
