//
// Copyright (c) 2018, Boris Popov <popov@whitekefir.ru>
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
//

#include "Name.hpp"
#include <memory>

#ifndef __cnflgr_Logger__
#define __cnflgr_Logger__

namespace cnflgr {
//////////////////////////////////////////////////////////////////
     class Logger;
     using logger_t = std::shared_ptr<Logger>;
//////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////
     class Logger {

     public:
	  Logger(void*);
	  Logger() = delete;
	  ~Logger();

     public:
	  void trace(string_t) const;
	  void debug(string_t) const;
	  void info(string_t) const;
	  void warn(string_t) const;
	  void error(string_t) const;
	  void fatal(string_t) const;

     private:
	  void* content;
	  bool check() const;
     };
//////////////////////////////////////////////////////////////////
}
#endif // __cnflgr_Logger__
