//
// Copyright (c) 2018, Boris Popov <popov@whitekefir.ru>
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
//

#include "String.hpp"

#ifndef __cnflgr_Name__
#define __cnflgr_Name__

namespace cnflgr {
//////////////////////////////////////////////////////////////////
     using name_t = string_t;
//////////////////////////////////////////////////////////////////
}
#endif // __cnflgr_Name__
