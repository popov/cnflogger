//
// Copyright (c) 2018, Boris Popov <popov@whitekefir.ru>
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
//

#include "Description.hpp"
#include <list>

#ifndef __cnflgr_Descriptions__
#define __cnflgr_Descriptions__

namespace cnflgr {
//////////////////////////////////////////////////////////////////
     using descriptions_t = std::list<description_t>;
//////////////////////////////////////////////////////////////////
}
#endif // __cnflgr_Descriptions__
