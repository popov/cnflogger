//
// Copyright (c) 2018, Boris Popov <popov@whitekefir.ru>
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
//

#include <cnflogger/Builder.hpp>
#include "Extractor.hpp"
#include "BuilderByDescription.hpp"

//////////////////////////////////////////////////////////////////
cnflgr::Builder::Builder(const cnf::Layer& l):
     layer(l)
{
     return;
}

void cnflgr::Builder::build() const {
     auto ds = Extractor::extract(layer);
     for (auto& i : ds) {
	  BuilderByDescription::build(i);
     }
}
////////////////////////////////////////////////////////////////// 
