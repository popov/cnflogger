//
// Copyright (c) 2018, Boris Popov <popov@whitekefir.ru>
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
//

#include "LoggerImpl.hpp"

//////////////////////////////////////////////////////////////////
cnflgr::LoggerImpl::LoggerImpl(log4cxx::LoggerPtr lp):
     content(lp)
{
     return;
}

void cnflgr::LoggerImpl::trace(string_t s) const {
     LOG4CXX_TRACE(content, s);
}

void cnflgr::LoggerImpl::debug(string_t s) const {
     LOG4CXX_DEBUG(content, s);
}

void cnflgr::LoggerImpl::info(string_t s) const {
     LOG4CXX_INFO(content, s);
}

void cnflgr::LoggerImpl::warn(string_t s) const {
     LOG4CXX_WARN(content, s);
}

void cnflgr::LoggerImpl::error(string_t s) const {
     LOG4CXX_ERROR(content, s);
}

void cnflgr::LoggerImpl::fatal(string_t s) const {
     LOG4CXX_FATAL(content, s);
}
////////////////////////////////////////////////////////////////// 
