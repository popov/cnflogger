//
// Copyright (c) 2018, Boris Popov <popov@whitekefir.ru>
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
//

#include "DescriptionPrint.hpp"
#include <ostream>

//////////////////////////////////////////////////////////////////
void cnflgr::DescriptionPrint::print(const description_t& d,
				     std::ostream& o) {
     o << ">>>>>>\n";
     o << "name: ";
     o << d.name << std::endl;
     o << "level: " << d.level << std::endl;
     print(d.appenders, o);
     o << "<<<<<<\n";
}

void cnflgr::DescriptionPrint::print(const appenders_t& a,
				     std::ostream& o) {
     for (auto& i : a) {
	  o << "---\n";
	  print(i, o);
	  o << "---\n";
     }
}

void cnflgr::DescriptionPrint::print(const appender_t& a,
				     std::ostream& o) {
     Appender* app = a.get();
     Console* c1 = dynamic_cast<Console*>(app);
     if (c1 != nullptr) print(c1, o);
     Syslog* c2 = dynamic_cast<Syslog*>(app);
     if (c2 != nullptr) print(c2, o);
     File* c3 = dynamic_cast<File*>(app);
     if (c3 != nullptr) print(c3, o);
     print(a->layout, o);
}

void cnflgr::DescriptionPrint::print(const Console* c,
				     std::ostream& o) {
     o << "CONSOLE ";
     if (c->sstm == OUT) {
	  o << "STDOUT";

     } else {
	  o << "STDERR";
     }
     o << std::endl;
}

void cnflgr::DescriptionPrint::print(const Syslog* s,
				     std::ostream& o) {
     o << "SYSLOG ";
     o << "FACILITY=" << s->facility;
     if (s->host.size() != 0) {
	  o << " HOST=" << s->host;
     }
     o << std::endl;
}

void cnflgr::DescriptionPrint::print(const File* f,
				     std::ostream& o) {
     o << "FILE ";
     print(f->info, o);
     o << std::endl;
}

void cnflgr::DescriptionPrint::print(const FileInfo& fi,
				     std::ostream& o) {
     o << "name=" << fi.name << " ";
     o << "append=" << fi.append;
     if (fi.buffered) {
	  o << " buffered=" << fi.buffered;
	  o << " size=" << fi.size;
     }
}

void cnflgr::DescriptionPrint::print(const layout_t& l,
				     std::ostream& o) {
     auto l1 = dynamic_cast<Simple*>(l.get());
     if (l1 != nullptr) print(l1, o);
     auto l2 = dynamic_cast<Pattern*>(l.get());
     if (l2 != nullptr) print(l2, o);
     auto l3 = dynamic_cast<HTML*>(l.get());
     if (l3 != nullptr) print(l3, o);
}

void cnflgr::DescriptionPrint::print(const Simple*,
				     std::ostream& o) {
     o << "SIMPLE";
     o << std::endl;
}

void cnflgr::DescriptionPrint::print(const Pattern* p,
				     std::ostream& o) {
     o << "PATTERN=";
     o << p->pattern();
     o << std::endl;
}
     
void cnflgr::DescriptionPrint::print(const HTML* h,
				     std::ostream& o) {
     o << "HTML ";
     o << "title=" << h->get_title();
     o << std::endl;
}
//////////////////////////////////////////////////////////////////
