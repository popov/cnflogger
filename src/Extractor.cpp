//
// Copyright (c) 2018, Boris Popov <popov@whitekefir.ru>
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
//

#include "Extractor.hpp"

//////////////////////////////////////////////////////////////////
cnflgr::descriptions_t
cnflgr::Extractor::extract(const cnf::Layer& lay) {
     descriptions_t ds;
     auto ns = get_logger_names(lay);
     for (auto& i : ns) {
	  description_t d;
	  if (make_description(lay, i, d))
	       ds.push_back(d);
     }
     return ds;
}

bool
cnflgr::Extractor::make_description(const cnf::Layer& l,
				    const name_t& n,
				    description_t& dsc) {
     dsc.name = n;
     auto ls = get_logger_section(l, n);
     if (! check_dict(ls)) return false;
     dsc.level = get_level(ls);
     auto app_names = get_app_names(ls);
     for (auto& i : app_names) {
	  auto app = make_appender(l, i);
	  dsc.appenders.push_back(app);
     }
     return true;
}

cnflgr::level_t
cnflgr::Extractor::get_level(const cnf::value_t& v) {
     using namespace cnf;
     Dict* d = (Dict*)(v.get());
     if (d->content.count("level") == 0) return ERROR;
     return to_level(d->content.at("level"));
}

std::list<cnflgr::string_t>
cnflgr::Extractor::get_app_names(const cnf::value_t& v) {
     using namespace cnf;
     std::list<string_t> res;
     Dict* d = (Dict*)(v.get());
     if (d->content.count("appenders") == 0) return res;
     auto ar = d->content.at("appenders");
     if (! check_array(ar)) return res;
     Array* arr = (Array*)(ar.get());
     if (arr->a_type != Value::STRING) return res;
     for (auto& i : arr->content) {
	  String* ss = (String*)(i.get());
	  res.push_back(ss->content);
     }
     return res;
}

cnflgr::level_t
cnflgr::Extractor::to_level(const cnf::value_t& v) {
     using namespace cnf;
     if (! check_string(v)) return ERROR;
     String* s = (String*)(v.get());
     auto ss = s->content;
     if      (ss == "UNDEF") return UNDEF;
     else if (ss == "TRACE") return TRACE;
     else if (ss == "DEBUG") return DEBUG;
     else if (ss == "INFO")  return INFO;
     else if (ss == "WARN")  return WARN;
     else if (ss == "ERROR") return ERROR;
     else if (ss == "FATAL") return FATAL;
     else                    return ERROR;
}

cnflgr::appender_t
cnflgr::Extractor::make_appender(const cnf::Layer& l,
				 string_t n) {
     auto as = get_app_sec(l, n);
     auto layout = make_layout(l, as);
     return _make_app(as, layout);
}

cnf::value_t
cnflgr::Extractor::get_app_sec(const cnf::Layer& l,
			       string_t s) {
     return get_sec(l, cnf::path_t{"logging", "appender", s});
}

cnf::value_t
cnflgr::Extractor::get_sec(const cnf::Layer& l,
			   cnf::path_t p) {
     using namespace cnf;
     value_t v;
     get(l, v, p);
     return v;
}

cnflgr::layout_t
cnflgr::Extractor::make_layout(const cnf::Layer& l,
			       const cnf::value_t& s) {
     auto name = get_layout_name(s);
     auto ss = get_layout_sec(l, name);
     auto t = get_type_layout(ss);
     if (t == "simple") {
	  return make_simple_layout();

     } else if (t == "pattern") {
	  return make_pattern_layout(ss);

     } else if (t == "html") {
	  return make_html_layout(ss);

     } else {
	  return make_simple_layout();
     }
}

cnflgr::layout_t
cnflgr::Extractor::make_simple_layout() {
     return std::make_shared<Simple>();
}

cnflgr::layout_t
cnflgr::Extractor::make_pattern_layout(const cnf::value_t& v) {
     auto p = get_string_on_key(v, "pattern");
     return std::make_shared<Pattern>(p);
}

cnflgr::layout_t
cnflgr::Extractor::make_html_layout(const cnf::value_t& v) {
     auto t = get_string_on_key(v, "title");
     auto h = std::make_shared<HTML>();
     h->set_title(t);
     return h;
}

cnflgr::string_t
cnflgr::Extractor::get_type_layout(const cnf::value_t& v) {
     return get_string_on_key(v, "type");
}

cnflgr::string_t
cnflgr::Extractor::get_layout_name(const cnf::value_t& v) {
     return get_string_on_key(v, "layout");
}

cnflgr::string_t
cnflgr::Extractor::get_string_on_key(const cnf::value_t& v,
				     string_t key) {
     using namespace cnf;
     if (! check_dict(v)) return "";
     Dict* d = (Dict*)(v.get());
     if (d->content.count(key) == 0) return "";
     auto vv = d->content.at(key);
     if (! check_string(vv)) return "";
     String* ss = (String*)(vv.get());
     return ss->content;
}

bool
cnflgr::Extractor::bool_on_key(const cnf::value_t& v,
			       string_t key,
			       bool& res) {
     using namespace cnf;
     if (! check_dict(v)) return false;
     Dict* d = (Dict*)(v.get());
     if (d->content.count(key) == 0) return false;
     auto vv = d->content.at(key);
     if (! check_bool(vv)) return false;
     Bool* ss = (Bool*)(vv.get());
     res = ss->content;
     return true;
}

cnf::value_t
cnflgr::Extractor::get_layout_sec(const cnf::Layer& l,
				  string_t s) {
     return get_sec(l, cnf::path_t{"logging", "layout", s});
}

cnflgr::appender_t
cnflgr::Extractor::_make_app(const cnf::value_t& v, layout_t l) {
     auto t = get_type_app(v);
     if (t == "console") {
	  return make_console_app(v, l);

     } else if (t == "syslog") {
	  auto vvv = make_syslog_app(v, l);
	  return vvv;

     } else if (t == "file") {
	  return make_file_app(v, l);

     } else {
	  return make_console_emer_app(l);
     }
}

cnflgr::appender_t
cnflgr::Extractor::make_console_app(const cnf::value_t& v,
				    layout_t l) {
     auto o = get_sys_out(v);
     return std::make_shared<Console>(l, o);
}

cnflgr::appender_t
cnflgr::Extractor::make_syslog_app(const cnf::value_t& v,
				   layout_t l) {
     string_t h = get_string_on_key(v, "host");
     if (h.size() == 0) return _make_syslog_app(v, l);
     return _make_syslog_app_host(v, l, h);
}

cnflgr::appender_t
cnflgr::Extractor::make_file_app(const cnf::value_t& v,
				 layout_t l) {
     auto fi = get_file_info(v);
     return std::make_shared<File>(l, fi);
}

cnflgr::FileInfo
cnflgr::Extractor::get_file_info(const cnf::value_t& v) {
     auto b = get_buffered(v);
     if (b) return get_file_info_buff(v);
     return get_file_info_no_buff(v);
}

cnflgr::FileInfo
cnflgr::Extractor::get_file_info_no_buff(const cnf::value_t& v) {
     auto a = get_append(v);
     auto f = get_filename(v);
     return FileInfo(f, a);
}

cnflgr::FileInfo
cnflgr::Extractor::get_file_info_buff(const cnf::value_t& v) {
     auto a = get_append(v);
     auto f = get_filename(v);
     auto s = get_size(v);
     return FileInfo(f, a, true, s);
}

cnflgr::string_t
cnflgr::Extractor::get_filename(const cnf::value_t& v) {
     return get_string_on_key(v, "filename");
}

bool cnflgr::Extractor::get_append(const cnf::value_t& v) {
     return get_bool(v, "append", true);
}

bool cnflgr::Extractor::get_buffered(const cnf::value_t& v) {
     return get_bool(v, "buffered", false);
}

int
cnflgr::Extractor::get_size(const cnf::value_t& v) {
     using namespace cnf;
     if (! check_dict(v)) return 1024;
     Dict* d = (Dict*)(v.get());
     if (d->content.count("buff_size") == 0) return 1024;
     auto vv = d->content.at("buff_size");
     if (vv->type != Value::INT) return 1024;
     Int* i = (Int*)(vv.get());
     return i->content;
}

bool
cnflgr::Extractor::get_bool(const cnf::value_t& v,
			    const string_t& key,
			    bool dfl) {
     bool result;
     if (bool_on_key(v, key, result))
	  return result;
     return dfl;
}

cnflgr::appender_t
cnflgr::Extractor::_make_syslog_app(const cnf::value_t& v,
				    layout_t l) {
     auto f = get_facility(v);
     return std::make_shared<Syslog>(l, f);
}

cnflgr::appender_t
cnflgr::Extractor::_make_syslog_app_host(const cnf::value_t& v,
					 layout_t l,
					 const host_t& h) {
     auto f = get_facility(v);
     return std::make_shared<Syslog>(l, h, f);
}

cnflgr::FACILITY
cnflgr::Extractor::get_facility(const cnf::value_t& v) {
     auto f = get_string_on_key(v, "facility");
     if (f == "LOG_AUTH")     return LOG_AUTH;
     if (f == "LOG_AUTHPRIV") return LOG_AUTHPRIV;
     if (f == "LOG_CRON")     return LOG_CRON;
     if (f == "LOG_DAEMON")   return LOG_DAEMON;
     if (f == "LOG_FTP")      return LOG_FTP;
     if (f == "LOG_KERN")     return LOG_KERN;
     if (f == "LOG_LOCAL0")   return LOG_LOCAL0;
     if (f == "LOG_LOCAL1")   return LOG_LOCAL1;
     if (f == "LOG_LOCAL2")   return LOG_LOCAL2;
     if (f == "LOG_LOCAL3")   return LOG_LOCAL3;
     if (f == "LOG_LOCAL4")   return LOG_LOCAL4;
     if (f == "LOG_LOCAL5")   return LOG_LOCAL5;
     if (f == "LOG_LOCAL6")   return LOG_LOCAL6;
     if (f == "LOG_LOCAL7")   return LOG_LOCAL7;
     if (f == "LOG_LPR")      return LOG_LPR;
     if (f == "LOG_MAIL")     return LOG_MAIL;
     if (f == "LOG_NEWS")     return LOG_NEWS;
     if (f == "LOG_SYSLOG")   return LOG_SYSLOG;
     if (f == "LOG_USER")     return LOG_USER;
     if (f == "LOG_UUCP")     return LOG_UUCP;
     return LOG_USER;
}

cnflgr::SYSTEM
cnflgr::Extractor::get_sys_out(const cnf::value_t& v) {
     auto out = get_string_on_key(v, "sys_out");
     if      (out == "stderr") return ERR;
     else if (out == "stdout") return OUT;
     return OUT;
}

cnflgr::string_t
cnflgr::Extractor::get_type_app(const cnf::value_t& v) {
     return get_string_on_key(v, "type");
}

cnflgr::appender_t
cnflgr::Extractor::make_console_emer_app(layout_t l) {
     return std::make_shared<Console>(l);
}

std::list<cnflgr::name_t>
cnflgr::Extractor::get_logger_names(const cnf::Layer& l) {
     using namespace cnf;
     std::list<name_t> names;
     value_t v;
     get(l, v, path_t{"logging", "loggers"});
     if (! check_array(v)) return names;
     Array* ar = (Array*)(v.get());
     if (ar->a_type != Value::STRING) return names;
     for (auto& i : ar->content) {
	  String* ars = (String*)(i.get());
	  names.push_back(ars->content);
     }
     return names;
}

cnf::value_t
cnflgr::Extractor::get_logger_section(const cnf::Layer& l,
				      const name_t& n) {
     return get_sec(l, cnf::path_t{"logging", "logger", n});
}

bool cnflgr::Extractor::check_dict(const cnf::value_t& v) {
     return check_(cnf::Value::DICT, v);
}

bool cnflgr::Extractor::check_array(const cnf::value_t& v) {
     return check_(cnf::Value::ARRAY, v);
}

bool cnflgr::Extractor::check_string(const cnf::value_t& v) {
     return check_(cnf::Value::STRING, v);
}

bool cnflgr::Extractor::check_bool(const cnf::value_t& v) {
     return check_(cnf::Value::BOOL, v);
}
////////////////////////////////////////////////////////////////// 
