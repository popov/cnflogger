//
// Copyright (c) 2018, Boris Popov <popov@whitekefir.ru>
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
//

#ifndef __cnflgr_Level__
#define __cnflgr_Level__

namespace cnflgr {
//////////////////////////////////////////////////////////////////
     enum level_t {
	  UNDEF = 0,
	  TRACE = 1,
	  DEBUG = 2,
	  INFO  = 3,
	  WARN  = 4,
	  ERROR = 5,
	  FATAL = 6,
     };
//////////////////////////////////////////////////////////////////
}
#endif // __cnflgr_Level__
