//
// Copyright (c) 2018, Boris Popov <popov@whitekefir.ru>
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
//

#include "LevelConverter.hpp"

//////////////////////////////////////////////////////////////////
log4cxx::LevelPtr cnflgr::LevelConverter::convert(level_t l) {
     switch (l) {
     case UNDEF:
     case TRACE:
	  return log4cxx::Level::getTrace();

     case DEBUG:
	  return log4cxx::Level::getDebug();

     case INFO:
	  return log4cxx::Level::getInfo();

     case WARN:
	  return log4cxx::Level::getWarn();

     case ERROR:
	  return log4cxx::Level::getError();

     case FATAL:
	  return log4cxx::Level::getFatal();
     };
     return log4cxx::Level::getError();
}
//////////////////////////////////////////////////////////////////
