//
// Copyright (c) 2018, Boris Popov <popov@whitekefir.ru>
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
//

#include "Layout.hpp"

//////////////////////////////////////////////////////////////////
cnflgr::Layout::Layout() {
     return;
}

cnflgr::Layout::~Layout() {
     return;
}
//////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////
cnflgr::Simple::Simple() {
     return;
}

cnflgr::Simple::~Simple() {
     return;
}
//////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////
cnflgr::Pattern::Pattern() {
     return;
}

cnflgr::Pattern::Pattern(pattern_t p):
     ptrn(p)
{
     return;
}

cnflgr::Pattern::~Pattern() {
     return;
}

cnflgr::pattern_t cnflgr::Pattern::pattern() const {
     return ptrn;
}
//////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////
cnflgr::HTML::HTML() {
     return;
}

cnflgr::HTML::~HTML() {
     return;
}

cnflgr::string_t cnflgr::HTML::get_title() const {
     return title;
}

void cnflgr::HTML::set_title(string_t s) {
     title = s;
}
//////////////////////////////////////////////////////////////////
