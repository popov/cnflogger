//
// Copyright (c) 2018, Boris Popov <popov@whitekefir.ru>
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
//

#include "Appender.hpp"

//////////////////////////////////////////////////////////////////
cnflgr::Appender::Appender():
     layout(std::make_shared<Simple>())
{
     return;
}

cnflgr::Appender::Appender(layout_t l):
     layout(l)
{
     return;
}

cnflgr::Appender::~Appender() {
     return;
}
//////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////
cnflgr::Console::Console():
     sstm(OUT)
{
     return;
}

cnflgr::Console::Console(layout_t l):
     Appender(l),
     sstm(OUT)
{
     return;
}

cnflgr::Console::Console(layout_t l, SYSTEM s):
     Appender(l),
     sstm(s)
{
     return;
}

cnflgr::Console::~Console() {
     return;
}

cnflgr::SYSTEM cnflgr::Console::get_target() const {
     return sstm;
}

void cnflgr::Console::set_target(SYSTEM s) {
     sstm = s;
}
//////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////
cnflgr::Syslog::Syslog():
     facility(LOG_USER)
{
     return;
}

cnflgr::Syslog::Syslog(FACILITY f):
     facility(f)
{
     return;
}

cnflgr::Syslog::Syslog(layout_t l, FACILITY f):
     Appender(l),
     facility(f)
{
     return;
}

cnflgr::Syslog::Syslog(layout_t l, host_t h, FACILITY f):
     Appender(l),
     facility(f),
     host(h)
{
     return;
}

cnflgr::Syslog::~Syslog() {
     return;
}
//////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////
cnflgr::FileInfo::FileInfo(filename_t fn):
     name(fn),
     append(false),
     buffered(false),
     size(0)
{
     return;
}

cnflgr::FileInfo::FileInfo(filename_t fn, append_t ap):
     name(fn),
     append(ap),
     buffered(false),
     size(0)
{
     return;
}

cnflgr::FileInfo::FileInfo(filename_t fn,
			   append_t ap,
			   buffered_t bf,
			   buffsize_t sz):
     name(fn),
     append(ap),
     buffered(bf),
     size(sz)
{
     return;
}
//////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////
cnflgr::File::File(FileInfo fi):
     info(fi)
{
     return;
}

cnflgr::File::File(layout_t l, FileInfo fi):
     Appender(l),
     info(fi)
{
     return;
}

cnflgr::File::~File() {
     return;
}
//////////////////////////////////////////////////////////////////
