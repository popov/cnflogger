//
// Copyright (c) 2018, Boris Popov <popov@whitekefir.ru>
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
//

#include <cnflogger/String.hpp>
#include <memory>

#ifndef __cnflgr_Layout__
#define __cnflgr_Layout__

namespace cnflgr {
//////////////////////////////////////////////////////////////////
     class Layout;
     using layout_t = std::shared_ptr<Layout>;
//////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////
     class Layout {

     public:
	  Layout();
	  virtual ~Layout() = 0;
     };
//////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////
     class Simple: public Layout {

     public:
	  Simple();
	  virtual ~Simple();
     };
//////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////
     using pattern_t = string_t;
     class Pattern: public Layout {

     public:
	  Pattern();
	  Pattern(pattern_t);
	  virtual ~Pattern();

	  virtual pattern_t pattern() const;

     protected:
	  pattern_t ptrn;
     };
//////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////
     class HTML: public Layout {

     public:
	  HTML();
	  virtual ~HTML();

	  virtual string_t get_title() const;
	  virtual void set_title(string_t);

     protected:
	  string_t title;
     };
//////////////////////////////////////////////////////////////////
}
#endif // __cnflgr_Layout__
