//
// Copyright (c) 2018, Boris Popov <popov@whitekefir.ru>
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
//

#include <cnflogger/Name.hpp>
#include "Level.hpp"
#include "Appenders.hpp"

#ifndef __cnflgr_Description__
#define __cnflgr_Description__

namespace cnflgr {
//////////////////////////////////////////////////////////////////
     struct description_t {
	  name_t      name;
	  level_t     level;
	  appenders_t appenders;
     };
//////////////////////////////////////////////////////////////////
}
#endif // __cnflgr_Description__
