//
// Copyright (c) 2018, Boris Popov <popov@whitekefir.ru>
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
//

#include "BuilderByDescription.hpp"
#include "LevelConverter.hpp"
#include "AppenderConverter.hpp"
#include <log4cxx/logger.h>

//////////////////////////////////////////////////////////////////
void
cnflgr::BuilderByDescription::build(const description_t& dsc) {
     using namespace log4cxx;
     auto l = Logger::getLogger(dsc.name);
     l->setLevel(LevelConverter::convert(dsc.level));
     for (auto& i : dsc.appenders) {
	  l->addAppender(AppenderConverter::convert(i));
     }
}
////////////////////////////////////////////////////////////////// 
