//
// Copyright (c) 2018, Boris Popov <popov@whitekefir.ru>
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
//

#include "AppenderConverter.hpp"
#include <log4cxx/consoleappender.h>
#include <log4cxx/fileappender.h>
#include <log4cxx/net/syslogappender.h>
#include <log4cxx/simplelayout.h>
#include <log4cxx/patternlayout.h>
#include <log4cxx/htmllayout.h>

//////////////////////////////////////////////////////////////////
cnflgr::AppenderConverter::facility_map_t
cnflgr::AppenderConverter::fm = {
     { LOG_AUTH,     "AUTH"     },
     { LOG_AUTHPRIV, "AUTHPRIV" },
     { LOG_CRON,     "CRON"     },
     { LOG_DAEMON,   "DAEMON"   },
     { LOG_FTP,      "FTP"      },
     { LOG_KERN,     "KERN"     },
     { LOG_LOCAL0,   "LOCAL0"   },
     { LOG_LOCAL1,   "LOCAL1"   },
     { LOG_LOCAL2,   "LOCAL2"   },
     { LOG_LOCAL3,   "LOCAL3"   },
     { LOG_LOCAL4,   "LOCAL4"   },
     { LOG_LOCAL5,   "LOCAL5"   },
     { LOG_LOCAL6,   "LOCAL6"   },
     { LOG_LOCAL7,   "LOCAL7"   },
     { LOG_LPR,      "LPR"      },
     { LOG_MAIL,     "MAIL"     },
     { LOG_NEWS,     "NEWS"     },
     { LOG_SYSLOG,   "SYSLOG"   },
     { LOG_USER,     "USER"     },
     { LOG_UUCP,     "UUCP"     },
};

log4cxx::AppenderPtr
cnflgr::AppenderConverter::convert(const appender_t& app) {
     Appender* A = app.get();
     auto ap = dynamic_cast<Console*>(A);
     if (ap != nullptr) return console(ap);
     auto aps = dynamic_cast<Syslog*>(A);
     if (aps != nullptr) return syslog(aps);
     auto apf = dynamic_cast<File*>(A);
     if (apf != nullptr) return file(apf);
     auto ca = new log4cxx::ConsoleAppender();
     return log4cxx::AppenderPtr(ca);
}

log4cxx::AppenderPtr
cnflgr::AppenderConverter::file(File* f) {
     if (f->info.buffered) return file_buffer(f);
     if (f->info.append) return file_append(f);
     return file_regular(f);
}

log4cxx::AppenderPtr
cnflgr::AppenderConverter::file_buffer(File* f) {
     using namespace log4cxx;
     auto log4cxx_layout = make_layout(f->layout);
     auto fa = new FileAppender(log4cxx_layout,
				f->info.name,
				f->info.append,
				f->info.buffered,
				f->info.size);
     return AppenderPtr(fa);
}

log4cxx::AppenderPtr
cnflgr::AppenderConverter::file_append(File* f) {
     using namespace log4cxx;
     auto log4cxx_layout = make_layout(f->layout);
     auto fa = new FileAppender(log4cxx_layout,
				f->info.name,
				f->info.append);
     return AppenderPtr(fa);
}

log4cxx::AppenderPtr
cnflgr::AppenderConverter::file_regular(File* f) {
     using namespace log4cxx;
     auto log4cxx_layout = make_layout(f->layout);
     auto fa = new FileAppender(log4cxx_layout,
				f->info.name);
     return AppenderPtr(fa);
}

log4cxx::AppenderPtr
cnflgr::AppenderConverter::syslog(Syslog* s) {
     if (s->host.size() != 0) return syslog_host(s);
     return syslog_no_host(s);
}

log4cxx::AppenderPtr
cnflgr::AppenderConverter::syslog_no_host(Syslog* s) {
     using namespace log4cxx;
     auto log4cxx_facility = get_facility(s->facility);
     auto log4cxx_layout = make_layout(s->layout);
     auto sa = new net::SyslogAppender(log4cxx_layout, log4cxx_facility);
     return AppenderPtr(sa);
}

log4cxx::AppenderPtr
cnflgr::AppenderConverter::syslog_host(Syslog* s) {
     using namespace log4cxx;
     auto log4cxx_facility = get_facility(s->facility);
     auto log4cxx_layout = make_layout(s->layout);
     auto host = s->host;
     auto sa = new net::SyslogAppender(log4cxx_layout,
				       host,
				       log4cxx_facility);
     return AppenderPtr(sa);
}

int cnflgr::AppenderConverter::get_facility(FACILITY f) {
     using namespace log4cxx::net;
     return SyslogAppender::getFacility(fm.at(f));
}

log4cxx::AppenderPtr
cnflgr::AppenderConverter::console(Console* apc) {
     auto layout = apc->layout;
     auto layout_cxx = make_layout(layout);
     using namespace log4cxx;
     auto ca = new ConsoleAppender(layout_cxx, get_target(apc->sstm));
     return AppenderPtr(ca);
}

log4cxx::LogString
cnflgr::AppenderConverter::get_target(SYSTEM s) {
     using namespace log4cxx;
     if (s == OUT) return ConsoleAppender::getSystemOut();
     return ConsoleAppender::getSystemErr();
}

log4cxx::LayoutPtr
cnflgr::AppenderConverter::make_layout(layout_t l) {
     Layout* L = l.get();
     auto ll1 = dynamic_cast<Simple*>(L);
     if (ll1 != nullptr) return simple();
     auto ll2 = dynamic_cast<Pattern*>(L);
     if (ll2 != nullptr) return pattern(ll2);
     auto ll3 = dynamic_cast<HTML*>(L);
     if (ll3 != nullptr) return html(ll3);
     return simple();
}

log4cxx::LayoutPtr
cnflgr::AppenderConverter::simple() {
     using namespace log4cxx;
     return LayoutPtr(new SimpleLayout);
}

log4cxx::LayoutPtr
cnflgr::AppenderConverter::pattern(Pattern* p) {
     using namespace log4cxx;
     return LayoutPtr(new PatternLayout(p->pattern()));
}

log4cxx::LayoutPtr
cnflgr::AppenderConverter::html(HTML* h) {
     using namespace log4cxx;
     auto hh = new HTMLLayout;
     hh->setTitle(h->get_title());
     return LayoutPtr(hh);
}
////////////////////////////////////////////////////////////////// 
