//
// Copyright (c) 2018, Boris Popov <popov@whitekefir.ru>
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
//

#include "Descriptions.hpp"
#include <cnf/Layer.hpp>
#include <cnf/LayerCommon.hpp>
#include <list>

#ifndef __cnflgr_Extractor__
#define __cnflgr_Extractor__

namespace cnflgr {
//////////////////////////////////////////////////////////////////
     class Extractor {

     public:
	  static descriptions_t extract(const cnf::Layer&);

     private:
	  static std::list<name_t>
	  get_logger_names(const cnf::Layer&);

	  static bool
	  make_description(const cnf::Layer&,
			   const name_t&,
			   description_t&);

	  static cnf::value_t
	  get_logger_section(const cnf::Layer&, const name_t&);

	  static level_t get_level(const cnf::value_t&);

	  static std::list<string_t>
	  get_app_names(const cnf::value_t&);

	  static appender_t
	  make_appender(const cnf::Layer&, string_t);

	  static cnf::value_t
	  get_app_sec(const cnf::Layer&, string_t);

	  static layout_t
	  make_layout(const cnf::Layer&, const cnf::value_t&);

	  static appender_t
	  _make_app(const cnf::value_t&, layout_t);

	  static layout_t
	  make_html_layout(const cnf::value_t&);

	  static layout_t
	  make_pattern_layout(const cnf::value_t&);

	  static layout_t make_simple_layout();

	  static string_t get_type_layout(const cnf::value_t&);

	  static string_t get_string_on_key(const cnf::value_t&,
					    string_t);

	  static cnf::value_t
	  get_layout_sec(const cnf::Layer&, string_t);

	  static cnf::value_t
	  get_sec(const cnf::Layer&, cnf::path_t);

	  static string_t get_layout_name(const cnf::value_t&);

	  static appender_t
	  make_console_emer_app(layout_t);

	  static appender_t
	  make_console_app(const cnf::value_t&, layout_t);

	  static appender_t
	  make_file_app(const cnf::value_t&, layout_t);

	  static FileInfo get_file_info(const cnf::value_t&);
	  static FileInfo get_file_info_buff(const cnf::value_t&);
	  static FileInfo get_file_info_no_buff(const cnf::value_t&);
	  static bool get_buffered(const cnf::value_t&);
	  static bool get_append(const cnf::value_t&);
	  static bool get_bool(const cnf::value_t&, const string_t&, bool);
	  static int get_size(const cnf::value_t&);
	  static string_t get_filename(const cnf::value_t&);


	  static appender_t
	  make_syslog_app(const cnf::value_t&, layout_t);

	  static appender_t
	  _make_syslog_app_host(const cnf::value_t&,
				layout_t,
				const host_t&);

	  static appender_t
	  _make_syslog_app(const cnf::value_t&, layout_t);

	  static FACILITY get_facility(const cnf::value_t&);

	  static SYSTEM get_sys_out(const cnf::value_t&);

	  static string_t get_type_app(const cnf::value_t&);

	  static bool check_dict(const cnf::value_t&);
	  static bool check_array(const cnf::value_t&);
	  static bool check_string(const cnf::value_t&);
	  static bool check_bool(const cnf::value_t&);
	  static level_t to_level(const cnf::value_t&);
	  static bool bool_on_key(const cnf::value_t&,
				  string_t,
				  bool&);

	  template<class A, class V>
	  static bool check_(const A& a, const V& v) {
	       if (! v) return false;
	       if (v->type != a) return false;
	       return true;
	  }
     };
//////////////////////////////////////////////////////////////////
}
#endif // __cnflgr_Extractor__
