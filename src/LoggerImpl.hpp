//
// Copyright (c) 2018, Boris Popov <popov@whitekefir.ru>
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
//

#include "cnflogger/String.hpp"
#include <log4cxx/logger.h>

#ifndef __cnflgr_LoggerImpl__
#define __cnflgr_LoggerImpl__

namespace cnflgr {
//////////////////////////////////////////////////////////////////
     class LoggerImpl {

     public:
	  LoggerImpl(log4cxx::LoggerPtr);

	  void trace(string_t) const;
	  void debug(string_t) const;
	  void info(string_t) const;
	  void warn(string_t) const;
	  void error(string_t) const;
	  void fatal(string_t) const;

     private:
	  log4cxx::LoggerPtr content;
     };
//////////////////////////////////////////////////////////////////
}
#endif // __cnflgr_LoggerImpl__
