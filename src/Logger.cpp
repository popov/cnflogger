//
// Copyright (c) 2018, Boris Popov <popov@whitekefir.ru>
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
//

#include <cnflogger/Logger.hpp>
#include "LoggerImpl.hpp"
#include <log4cxx/logger.h>

//////////////////////////////////////////////////////////////////
cnflgr::Logger::Logger(void* p):
     content(p)
{
     return;
}

cnflgr::Logger::~Logger() {
     if (! check()) return;
     LoggerImpl* li = (LoggerImpl*)content;
     delete li;
}

bool cnflgr::Logger::check() const {
     if (content == nullptr) return false;
     return true;
}

void cnflgr::Logger::trace(string_t s) const {
     if (! check()) return;
     LoggerImpl* li = (LoggerImpl*)content;
     li->trace(s);
}

void cnflgr::Logger::debug(string_t s) const {
     if (! check()) return;
     LoggerImpl* li = (LoggerImpl*)content;
     li->debug(s);
}

void cnflgr::Logger::info(string_t s) const {
     if (! check()) return;
     LoggerImpl* li = (LoggerImpl*)content;
     li->info(s);
}

void cnflgr::Logger::warn(string_t s) const {
     if (! check()) return;
     LoggerImpl* li = (LoggerImpl*)content;
     li->warn(s);
}

void cnflgr::Logger::error(string_t s) const {
     if (! check()) return;
     LoggerImpl* li = (LoggerImpl*)content;
     li->error(s);
}

void cnflgr::Logger::fatal(string_t s) const {
     if (! check()) return;
     LoggerImpl* li = (LoggerImpl*)content;
     li->fatal(s);
}
//////////////////////////////////////////////////////////////////
