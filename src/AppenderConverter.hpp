//
// Copyright (c) 2018, Boris Popov <popov@whitekefir.ru>
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
//

#include "Appender.hpp"
#include <log4cxx/logger.h>
#include <map>

#ifndef __cnflgr_AppenderConverter__
#define __cnflgr_AppenderConverter__

namespace cnflgr {
//////////////////////////////////////////////////////////////////
     class AppenderConverter {

     public:
	  static log4cxx::AppenderPtr convert(const appender_t&);

     private:
	  static log4cxx::AppenderPtr console(Console*);
	  static log4cxx::AppenderPtr syslog(Syslog*);
	  static log4cxx::AppenderPtr file(File*);
	  static log4cxx::AppenderPtr file_buffer(File*);
	  static log4cxx::AppenderPtr file_append(File*);
	  static log4cxx::AppenderPtr file_regular(File*);
	  static log4cxx::AppenderPtr syslog_host(Syslog*);
	  static log4cxx::AppenderPtr syslog_no_host(Syslog*);
	  static log4cxx::LayoutPtr make_layout(layout_t);
	  static log4cxx::LayoutPtr simple();
	  static log4cxx::LayoutPtr pattern(Pattern*);
	  static log4cxx::LayoutPtr html(HTML*);
	  static log4cxx::LogString get_target(SYSTEM);
	  static int get_facility(FACILITY);

     private:
	  using facility_map_t = std::map<FACILITY, string_t>;
	  static facility_map_t fm;
     };
//////////////////////////////////////////////////////////////////
}
#endif // __cnflgr_AppenderConverter__
