//
// Copyright (c) 2018, Boris Popov <popov@whitekefir.ru>
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
//

#include "Layout.hpp"
#include <memory>

#ifndef __cnflgr_Appender__
#define __cnflgr_Appender__

namespace cnflgr {
//////////////////////////////////////////////////////////////////
     class Appender;
     using appender_t = std::shared_ptr<Appender>;
//////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////
     class Appender {

     public:
	  friend class AppenderConverter;
	  friend class DescriptionPrint;

     public:
	  Appender();
	  Appender(layout_t);
	  virtual ~Appender() = 0;

     protected:
	  layout_t layout;
     };
//////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////
     enum SYSTEM {
	  OUT = 1,
	  ERR = 2,
     };
//////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////
     class Console: public Appender {

     public:
	  friend class AppenderConverter;
	  friend class DescriptionPrint;

     public:
	  Console();
	  Console(layout_t);
	  Console(layout_t, SYSTEM);
	  virtual ~Console();

	  virtual SYSTEM get_target() const;
	  virtual void set_target(SYSTEM);

     protected:
	  SYSTEM sstm;
     };
//////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////
     enum FACILITY {
	  LOG_AUTH     =  1,
	  LOG_AUTHPRIV =  2,
	  LOG_CRON     =  3,
	  LOG_DAEMON   =  4,
	  LOG_FTP      =  5,
	  LOG_KERN     =  6,
	  LOG_LOCAL0   =  7,
	  LOG_LOCAL1   =  8,
	  LOG_LOCAL2   =  9,
	  LOG_LOCAL3   = 10,
	  LOG_LOCAL4   = 11,
	  LOG_LOCAL5   = 12,
	  LOG_LOCAL6   = 13,
	  LOG_LOCAL7   = 14,
	  LOG_LPR      = 15,
	  LOG_MAIL     = 16,
	  LOG_NEWS     = 17,
	  LOG_SYSLOG   = 18,
	  LOG_USER     = 19,
	  LOG_UUCP     = 20,
     };
//////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////
     using host_t = string_t;
//////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////
     class Syslog: public Appender {

     public:
	  friend class AppenderConverter;
	  friend class DescriptionPrint;

     public:
	  Syslog();
	  Syslog(FACILITY);
	  Syslog(layout_t, FACILITY);
	  Syslog(layout_t, host_t, FACILITY);
	  virtual ~Syslog();

     protected:
	  const FACILITY facility;
	  const host_t host;
     };
//////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////
     using filename_t = string_t;
     using append_t = bool;
     using buffered_t = bool;
     using buffsize_t = int;
//////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////
     class FileInfo {

     public:
	  friend class AppenderConverter;
	  friend class DescriptionPrint;

     public:
	  FileInfo(filename_t);
	  FileInfo(filename_t, append_t);
	  FileInfo(filename_t, append_t, buffered_t, buffsize_t);

     private:
	  const filename_t name;
	  const append_t append;
	  const buffered_t buffered;
	  const buffsize_t size;
     };
//////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////
     class File: public Appender {

     public:
	  friend class AppenderConverter;
	  friend class DescriptionPrint;

     public:
	  File(FileInfo);
	  File(layout_t, FileInfo);
	  virtual ~File();

     protected:
	  const FileInfo info;
     };
//////////////////////////////////////////////////////////////////
}
#endif // __cnflgr_Appender__
