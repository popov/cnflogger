//
// Copyright (c) 2018, Boris Popov <popov@whitekefir.ru>
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
//

#include "Appender.hpp"
#include <list>

#ifndef __cnflgr_Appenders__
#define __cnflgr_Appenders__

namespace cnflgr {
//////////////////////////////////////////////////////////////////
     using appenders_t = std::list<appender_t>;
//////////////////////////////////////////////////////////////////
}
#endif // __cnflgr_Appenders__
