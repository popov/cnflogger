//
// Copyright (c) 2018, Boris Popov <popov@whitekefir.ru>
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
//

#include <cnflogger/Holder.hpp>
#include "LoggerImpl.hpp"

//////////////////////////////////////////////////////////////////
cnflgr::logger_t cnflgr::Holder::get(name_t name) {
     log4cxx::LoggerPtr logger(log4cxx::Logger::getLogger(name));
     auto li = new LoggerImpl(logger);
     return std::make_shared<Logger>(li);
}
////////////////////////////////////////////////////////////////// 
