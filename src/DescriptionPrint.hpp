//
// Copyright (c) 2018, Boris Popov <popov@whitekefir.ru>
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
//

#include "Description.hpp"

#ifndef __cnflgr_DescriptionPrint__
#define __cnflgr_DescriptionPrint__

namespace cnflgr {
//////////////////////////////////////////////////////////////////
     class DescriptionPrint {

     public:
	  static void print(const description_t&,
			    std::ostream&);
     private:
	  static void print(const appenders_t&,
			    std::ostream&);
	  static void print(const appender_t&,
			    std::ostream&);
	  static void print(const layout_t&,
			    std::ostream&);
	  static void print(const Console*, std::ostream&);
	  static void print(const Syslog*, std::ostream&);
	  static void print(const File*, std::ostream&);
	  static void print(const FileInfo&, std::ostream&);
	  static void print(const Simple*, std::ostream&);
	  static void print(const Pattern*, std::ostream&);
	  static void print(const HTML*, std::ostream&);
     };
//////////////////////////////////////////////////////////////////
}
#endif // __cnflgr_DescriptionPrint__
