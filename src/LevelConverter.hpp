//
// Copyright (c) 2018, Boris Popov <popov@whitekefir.ru>
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
//

#include "Level.hpp"
#include <log4cxx/logger.h>

#ifndef __cnflgr_LevelConverter__
#define __cnflgr_LevelConverter__

namespace cnflgr {
//////////////////////////////////////////////////////////////////
     class LevelConverter {

     public:
	  static log4cxx::LevelPtr convert(level_t);
     };
//////////////////////////////////////////////////////////////////
}
#endif // __cnflgr_LevelConverter__
