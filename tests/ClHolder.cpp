#include <cnflogger/Holder.hpp>
#include <gtest/gtest.h>

using namespace cnflgr;

TEST(ClHolder, test_01)
{
     auto lgr = Holder::get("");
     lgr->trace("trace");
     lgr->debug("debug");
     lgr->info("info");
     lgr->warn("warn");
     lgr->error("error");
     lgr->fatal("fatal");
}

TEST(ClHolder, test_02)
{
     auto lgr = Holder::get("");
     lgr->trace("trace");
     lgr->debug("debug");
     lgr->info("info");
     lgr->warn("warn");
     lgr->error("error");
     lgr->fatal("fatal");
}

TEST(ClHolder, test_03)
{
     auto lgr = Holder::get("x");
     lgr->trace("trace");
     lgr->debug("debug");
     lgr->info("info");
     lgr->warn("warn");
     lgr->error("error");
     lgr->fatal("fatal");
}
