#include <Appender.hpp>
#include <gtest/gtest.h>

using namespace cnflgr;

TEST(ClAppender, test_01)
{
     auto o = OUT;
     ASSERT_EQ(1, o);
}

TEST(ClAppender, test_02)
{
     auto o = ERR;
     ASSERT_EQ(2, o);
}

TEST(ClAppender, test_03)
{
     appender_t a = std::make_shared<Console>();
     Console* c = (Console*)(a.get());
     ASSERT_EQ(OUT, c->get_target());
     c->set_target(ERR);
     auto t = c->get_target();
     ASSERT_EQ(ERR, t);
}

TEST(ClAppender, test_04)
{
     auto h = std::make_shared<HTML>();
     appender_t a = std::make_shared<Console>(h);
     Console* c = (Console*)(a.get());
     ASSERT_EQ(OUT, c->get_target());
}

TEST(ClAppender, test_05)
{
     auto h = std::make_shared<HTML>();
     appender_t a = std::make_shared<Console>(h, ERR);
     Console* c = (Console*)(a.get());
     ASSERT_EQ(ERR, c->get_target());
}

TEST(ClAppender, test_06)
{
     //auto l = std::make_shared<Simple>();
     auto a = std::make_shared<Syslog>(LOG_LOCAL0);
}

TEST(ClAppender, test_07)
{
     auto l = std::make_shared<Simple>();
     auto a = std::make_shared<Syslog>(l, LOG_LOCAL0);
}

TEST(ClAppender, test_08)
{
     auto l = std::make_shared<Simple>();
     auto a = std::make_shared<Syslog>(l, "dabbler", LOG_LOCAL0);
}

TEST(ClAppender, test_09)
{
     auto a = std::make_shared<Syslog>();
}

TEST(ClAppender, test_10)
{
     //auto a = std::make_shared<Syslog>();
     FileInfo fi("test_file");
}

TEST(ClAppender, test_11)
{
     //auto a = std::make_shared<Syslog>();
     FileInfo fi("test_file", true);
}

TEST(ClAppender, test_12)
{
     //auto a = std::make_shared<Syslog>();
     FileInfo fi("test_file", true, true, 4096);
}

TEST(ClAppender, test_13)
{
     FileInfo fi("test_file");
     auto a = std::make_shared<File>(fi);
}

TEST(ClAppender, test_14)
{
     auto l = std::make_shared<HTML>();
     FileInfo fi("test_file");
     auto a = std::make_shared<File>(l, fi);
}
