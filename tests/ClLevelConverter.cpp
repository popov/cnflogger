#include <LevelConverter.hpp>
#include <gtest/gtest.h>

using namespace cnflgr;

TEST(ClLevelConverter, test_01)
{
     using namespace log4cxx;
     auto i = LevelConverter::convert(INFO);
     auto a = i->toString();
     ASSERT_EQ("INFO", a);
}

TEST(ClLevelConverter, test_02)
{
     using namespace log4cxx;
     auto i = LevelConverter::convert(UNDEF);
     auto a = i->toString();
     ASSERT_EQ("TRACE", a);
}

TEST(ClLevelConverter, test_03)
{
     using namespace log4cxx;
     auto i = LevelConverter::convert(TRACE);
     auto a = i->toString();
     ASSERT_EQ("TRACE", a);
}

TEST(ClLevelConverter, test_04)
{
     using namespace log4cxx;
     auto i = LevelConverter::convert(DEBUG);
     auto a = i->toString();
     ASSERT_EQ("DEBUG", a);
}

TEST(ClLevelConverter, test_05)
{
     using namespace log4cxx;
     auto i = LevelConverter::convert(WARN);
     auto a = i->toString();
     ASSERT_EQ("WARN", a);
}

TEST(ClLevelConverter, test_06)
{
     using namespace log4cxx;
     auto i = LevelConverter::convert(FATAL);
     auto a = i->toString();
     ASSERT_EQ("FATAL", a);
}

TEST(ClLevelConverter, test_051)
{
     using namespace log4cxx;
     auto i = LevelConverter::convert(WARN);
     auto a = i->toString();
     ASSERT_EQ("WARN", a);
}

TEST(ClLevelConverter, test_061)
{
     using namespace log4cxx;
     auto i = LevelConverter::convert(FATAL);
     auto a = i->toString();
     ASSERT_EQ("FATAL", a);
}
