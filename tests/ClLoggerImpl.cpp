#include <LoggerImpl.hpp>
#include <gtest/gtest.h>

using namespace cnflgr;

TEST(ClLoggerImpl, test_01)
{
     log4cxx::LoggerPtr logger(log4cxx::Logger::getLogger("test"));
     LoggerImpl li(logger);
     li.trace("trace");
     li.debug("debug");
     li.info("info");
     li.warn("warn");
     li.error("error");
     li.fatal("fatal");
}

TEST(ClLoggerImpl, test_02)
{
     log4cxx::LoggerPtr logger(log4cxx::Logger::getLogger("test2"));
     LoggerImpl li(logger);
     li.trace("trace");
     li.debug("debug");
     li.info("info");
     li.warn("warn");
     li.error("error");
     li.fatal("fatal");
}

TEST(ClLoggerImpl, test_03)
{
     log4cxx::LoggerPtr logger(log4cxx::Logger::getLogger("test"));
     LoggerImpl li(logger);
     li.trace("trace");
     li.debug("debug");
     li.info("info");
     li.warn("warn");
     li.error("error");
     li.fatal("fatal");
}
