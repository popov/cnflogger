#include <cnf/LayerCommon.hpp>
#include <cnflogger/Builder.hpp>
#include <cnflogger/Holder.hpp>
#include <gtest/gtest.h>

using namespace cnflgr;

std::string input01(R"foo(
[logging]
loggers = [ "one" ]

[logging.logger."one"]
level = "DEBUG"
appenders =  [ "1", "2" ]

[logging.appender."1"]
type = "console"
sys_out = "stderr"
layout = "1"

[logging.appender."2"]
type = "syslog"
facility = "LOG_USER"
layout = "2"

[logging.layout."1"]
type = "simple"

[logging.layout."2"]
type = "pattern"
#pattern = "\\%c \\%d{dd MMM yyyy HH:mm:ss,SSS} \\%m \\%n"
pattern = "%c %d{dd MMM yyyy HH:mm:ss,SSS} %m %n"
)foo");
TEST(ClOne, test_01)
{
     std::stringstream s01(input01);
     cnf::Layer l1(1);
     ASSERT_TRUE(cnf::build(s01, l1));
     Builder B(l1);
     B.build();

     auto lgr = Holder::get("one");
     lgr->trace("one: trace");
     lgr->debug("one: debug");
     lgr->info("one: info");
     lgr->warn("one: warn");
     lgr->error("one: error");
     lgr->fatal("one: fatal");

     auto lgr2 = Holder::get("one.two");
     lgr2->trace("one.two: trace");
     lgr2->debug("one.two: debug");
     lgr2->info("one.two: info");
     lgr2->warn("one.two: warn");
     lgr2->error("one.two: error");
     lgr2->fatal("one.two: fatal");
}
