#include <Extractor.hpp>
#include <DescriptionPrint.hpp>
#include <cnf/LayerCommon.hpp>
#include <gtest/gtest.h>

using namespace cnflgr;

std::string input01(R"foo(
[logging]
loggers = [ "one", "two", "three" ]

[logging.logger."one"]
level = "DEBUG"
appenders =  [ "1", "2", "3" ]

[logging.logger."two"]
level = "INFO"
appenders =  [ "1", "2" ]

[logging.logger."three"]
level = "WARN"
appenders =  [ "3" ]

[logging.appender."1"]
type = "console"
sys_out = "stderr"
layout = "1"

[logging.appender."12"]
type = "console"
sys_out = "stderr"
layout = "3"

[logging.appender."2"]
type = "syslog"
facility = "LOG_MAIL"
host = "dabbler"
layout = "3"

[logging.appender."3"]
type = "file"
filename = "_filename_"
append = true
buffered = true
buff_size = 1024
layout = "2"

[logging.layout."1"]
type = "simple"

[logging.layout."2"]
type = "pattern"
pattern = "\\%c \\%d{dd MMM yyyy HH:mm:ss,SSS} \\%m \\%n"

[logging.layout."3"]
type = "html"
title = "test"
)foo");
std::string etalon01(R"foo(>>>>>>
name: one
level: 2
---
CONSOLE STDERR
SIMPLE
---
---
SYSLOG FACILITY=16 HOST=dabbler
HTML title=test
---
---
FILE name=_filename_ append=1 buffered=1 size=1024
PATTERN=\%c \%d{dd MMM yyyy HH:mm:ss,SSS} \%m \%n
---
<<<<<<
>>>>>>
name: two
level: 3
---
CONSOLE STDERR
SIMPLE
---
---
SYSLOG FACILITY=16 HOST=dabbler
HTML title=test
---
<<<<<<
>>>>>>
name: three
level: 4
---
FILE name=_filename_ append=1 buffered=1 size=1024
PATTERN=\%c \%d{dd MMM yyyy HH:mm:ss,SSS} \%m \%n
---
<<<<<<
)foo");

TEST(ClExtractor, test_01)
{
     std::stringstream s01(input01);
     cnf::Layer l1(1);
     cnf::build(s01, l1);
     ASSERT_EQ(1, l1.dict.size());
}

std::string input02(R"foo(
[logging]
loggers = [ "one" ]

[logging.logger."one"]
level = "DEBUG"
appenders =  [ "2" ]

[logging.appender."2"]
type = "syslog"
facility = "LOG_USER"
#host = "dabbler"
layout = "3"

[logging.layout."3"]
type = "html"
title = "test"
)foo");
std::string etalon02(R"foo(>>>>>>
name: one
level: 2
---
SYSLOG FACILITY=19
HTML title=test
---
<<<<<<
)foo");
TEST(ClExtractor, test_02)
{
     std::stringstream s01(input02);
     cnf::Layer l1(1);
     cnf::build(s01, l1);
     auto ds = Extractor::extract(l1);
     ASSERT_EQ(1, ds.size());
     std::stringstream sss;
     for (auto i : ds)
     	  DescriptionPrint::print(i, sss);
     // std::cerr << sss.str();
     ASSERT_EQ(etalon02, sss.str());
}

std::string input03(R"foo(
[logging]
loggers = [ "one" ]

[logging.logger."one"]
level = "DEBUG"
appenders =  [ "2" ]

[logging.appender."2"]
type = "syslog"
facility = "LOG_USER"
host = "dabbler"
layout = "3"

[logging.layout."3"]
type = "html"
title = "test"
)foo");
std::string etalon03(R"foo(>>>>>>
name: one
level: 2
---
SYSLOG FACILITY=19 HOST=dabbler
HTML title=test
---
<<<<<<
)foo");
TEST(ClExtractor, test_03)
{
     std::stringstream s01(input03);
     cnf::Layer l1(1);
     cnf::build(s01, l1);
     auto ds = Extractor::extract(l1);
     ASSERT_EQ(1, ds.size());
     std::stringstream sss;
     for (auto i : ds)
     	  DescriptionPrint::print(i, sss);
     // std::cerr << sss.str();
     ASSERT_EQ(etalon03, sss.str());
}

TEST(ClExtractor, test_04)
{
     std::stringstream s01(input01);
     cnf::Layer l1(1);
     cnf::build(s01, l1);
     auto ds = Extractor::extract(l1);
     ASSERT_EQ(3, ds.size());
     std::stringstream sss;
     for (auto i : ds)
     	  DescriptionPrint::print(i, sss);
     //std::cerr << sss.str();
     ASSERT_EQ(etalon01, sss.str());
}
