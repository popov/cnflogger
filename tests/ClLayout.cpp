#include <Layout.hpp>
#include <gtest/gtest.h>

using namespace cnflgr;

TEST(ClLayout, test_01)
{
     layout_t l = std::make_shared<Simple>();
     ASSERT_NE(nullptr, l.get());
}

TEST(ClLayout, test_02)
{
     layout_t l = std::make_shared<Pattern>();
     auto ll = (Pattern*)(l.get());
     auto p = ll->pattern();
     ASSERT_EQ("", p);
}

TEST(ClLayout, test_03)
{
     layout_t l = std::make_shared<Pattern>("zxc");
     auto ll = (Pattern*)(l.get());
     auto p = ll->pattern();
     ASSERT_EQ("zxc", p);
}

TEST(ClLayout, test_04)
{
     layout_t l = std::make_shared<HTML>();
     auto ll = (HTML*)(l.get());
     auto p = ll->get_title();
     ASSERT_EQ("", p);
     ll->set_title("XXX");
     p = ll->get_title();
     ASSERT_EQ("XXX", p);
}
