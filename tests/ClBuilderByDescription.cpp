#include <BuilderByDescription.hpp>
#include <cnflogger/Holder.hpp>
#include <gtest/gtest.h>

using namespace cnflgr;

TEST(ClBuilderByDescription, test_01)
{
     description_t d;
     d.name = "Xx";
     d.level = TRACE;
     d.appenders = { std::make_shared<Console>() };
     BuilderByDescription::build(d);
}

TEST(ClBuilderByDescription, test_02)
{
     auto l = Holder::get("Xx");
     l->trace("one: trace");
     l->debug("one: debug");
     l->info("one: info");
     l->warn("one: warn");
     l->error("one: error");
     l->fatal("one: fatal");
}

TEST(ClBuilderByDescription, test_03)
{
     auto l = Holder::get("Xx.one");
     l->trace("Xone: trace");
     l->debug("Xone: debug");
     l->info("Xone: info");
     l->warn("Xone: warn");
     l->error("Xone: error");
     l->fatal("Xone: fatal");
}

TEST(ClBuilderByDescription, test_04)
{
     description_t d;
     d.name = "test_04";
     d.level = TRACE;
     auto c = std::make_shared<Console>();
     c->set_target(ERR);
     d.appenders = { c };
     BuilderByDescription::build(d);
}

TEST(ClBuilderByDescription, test_05)
{
     auto l = Holder::get("test_04");
     l->trace("test_04: trace");
     l->debug("test_04: debug");
     l->info("test_04: info");
     l->warn("test_04: warn");
     l->error("test_04: error");
     l->fatal("test_04: fatal");
}

TEST(ClBuilderByDescription, test_06)
{
     description_t d;
     d.name = "test_06";
     d.level = TRACE;
     auto l = std::make_shared<HTML>();
     l->set_title("STARS");
     auto c = std::make_shared<Console>(l);
     //c->set_target(ERR);
     d.appenders = { c };
     BuilderByDescription::build(d);
}

TEST(ClBuilderByDescription, test_07)
{
     auto l = Holder::get("test_06");
     l->trace("test_06: trace");
     l->debug("test_06: debug");
     l->info("test_06: info");
     l->warn("test_06: warn");
     l->error("test_06: error");
     l->fatal("test_06: fatal");
}

TEST(ClBuilderByDescription, test_08)
{
     description_t d;
     d.name = "test_08";
     d.level = TRACE;
     auto l = std::make_shared<Pattern>
	  ("\%c \%d{dd MMM yyyy HH:mm:ss,SSS} \%m \%n");
//	  ("\%c \%d{K:mm a, z} \%m \%n");
     auto c = std::make_shared<Console>(l);
     d.appenders = { c };
     BuilderByDescription::build(d);
}

TEST(ClBuilderByDescription, test_09)
{
     auto l = Holder::get("test_08");
     l->trace("trace");
     l->debug("debug");
     l->info("info");
     l->warn("warn");
     l->error("error");
     l->fatal("fatal");
}

TEST(ClBuilderByDescription, test_10)
{
     description_t d;
     d.name = "test_10";
     d.level = TRACE;
     auto l = std::make_shared<Pattern>
 	  ("\%c \%d{dd MMM yyyy HH:mm:ss,SSS} \%m \%n");
// //	  ("\%c \%d{K:mm a, z} \%m \%n");
     auto c1 = std::make_shared<Syslog>(l, LOG_USER);
     auto c2 = std::make_shared<Console>(l);
     d.appenders = { c1, c2 };
     BuilderByDescription::build(d);
}

TEST(ClBuilderByDescription, test_11)
{
     auto l = Holder::get("test_10");
     l->trace("trace");
     l->debug("debug");
     l->info("info");
     l->warn("warn");
     l->error("error");
}

TEST(ClBuilderByDescription, test_12)
{
     description_t d;
     d.name = "test_12";
     d.level = TRACE;
     auto l = std::make_shared<Pattern>
 	  ("\%c \%d{dd MMM yyyy HH:mm:ss,SSS} \%m \%n");
     // //	  ("\%c \%d{K:mm a, z} \%m \%n");
     FileInfo fi("test_log_file.log");
     auto c1 = std::make_shared<File>(l, fi);
     auto c2 = std::make_shared<Console>(l);
     d.appenders = { c1, c2 };
     BuilderByDescription::build(d);
}

TEST(ClBuilderByDescription, test_13)
{
     auto l = Holder::get("test_12");
     l->trace("trace");
     l->debug("debug");
     l->info("info");
     l->warn("warn");
     l->error("error");
}
