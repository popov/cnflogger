#
# LOG4CXX_FOUND
# LOG4CXX_INCLUDE_DIR
# LOG4CXX_LIBRARY
#

FIND_PATH(LOG4CXX_INCLUDE_DIR "log4cxx/logger.h" PATHS
  ENV "LOG4CXX_INCLUDE_DIR"
  /include
  /usr/include
  /usr/local/include
  /opt/include
  /opt/local/include
  ~/include
  NO_DEFAULT_PATH
  )

FIND_LIBRARY(LOG4CXX_LIBRARY log4cxx PATHS
  ENV "LOG4CXX_LIB_DIR"
  ~/lib
  /lib
  /usr/lib
  /usr/local/lib
  /opt/lib
  /opt/local/lib
  PATH_SUFFIXES
  /lib/
  /lib64/
  /x86_32-linux-gnu/
  /x86_64-linux-gnu/
  NO_DEFAULT_PATH
  )

SET(LOG4CXX_FOUND 0)

IF(LOG4CXX_INCLUDE_DIR AND LOG4CXX_LIBRARY)
  SET(LOG4CXX_FOUND 1)
  message("LOG4CXX headers dir  : ${LOG4CXX_INCLUDE_DIR}")
  message("LOG4CXX library found: ${LOG4CXX_LIBRARY}")
else(LOG4CXX_INCLUDE_DIR AND LOG4CXX_LIBRARY)
  message("NOT FOUND LOG4CXX!")  
ENDIF(LOG4CXX_INCLUDE_DIR AND LOG4CXX_LIBRARY)
