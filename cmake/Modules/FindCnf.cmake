#
# CNF_FOUND
# CNF_INCLUDE_DIR
# CNF_LIBRARY
#

FIND_PATH(CNF_INCLUDE_DIR "cnf/Configuration.hpp" PATHS
  ENV "CNF_INCLUDE_DIR"
  /include
  /usr/include
  /usr/local/include
  /opt/include
  /opt/local/include
  ~/include
  NO_DEFAULT_PATH
  )

FIND_LIBRARY(CNF_LIBRARY cnf PATHS
  ENV "CNF_LIB_DIR"
  ~/lib
  /lib
  /usr/lib
  /usr/local/lib
  /opt/lib
  /opt/local/lib
  PATH_SUFFIXES
  /lib/
  /lib64/
  /x86_32-linux-gnu/
  /x86_64-linux-gnu/
  NO_DEFAULT_PATH
  )

SET(CNF_FOUND 0)

IF(CNF_INCLUDE_DIR AND CNF_LIBRARY)
  SET(CNF_FOUND 1)
  message("CNF headers dir  : ${CNF_INCLUDE_DIR}")
  message("CNF library found: ${CNF_LIBRARY}")
else(CNF_INCLUDE_DIR AND CNF_LIBRARY)
  message("NOT FOUND CNF!")  
ENDIF(CNF_INCLUDE_DIR AND CNF_LIBRARY)
